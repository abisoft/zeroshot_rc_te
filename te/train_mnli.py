"""
Modified from https://github.com/nyu-mll/multiNLI
"""

import tensorflow as tf
import os,math
import importlib
import random
import datetime
import utils.parameters as params
from utils.data_processing import *
from utils.evaluate import *

from te.model import MyModel

class modelClassifier:
    def __init__(self,FIXED_PARAMETERS):
        ## Define hyperparameters
        seq_length = FIXED_PARAMETERS["seq_length"]
        self.learning_rate =  FIXED_PARAMETERS["learning_rate"]
        self.display_epoch_freq =  FIXED_PARAMETERS["display_epoch_freq"]
        # self.display_step_freq = 100
        # self.display_step_freq = 1
        self.embedding_dim = FIXED_PARAMETERS["word_embedding_dim"]
        self.dim = FIXED_PARAMETERS["hidden_embedding_dim"]
        self.batch_size = FIXED_PARAMETERS["batch_size"]
        self.emb_train = FIXED_PARAMETERS["emb_train"]
        self.emb_train_topn = FIXED_PARAMETERS["emb_train_topn"]
        self.keep_rate = FIXED_PARAMETERS["keep_rate"]
        self.sequence_length = FIXED_PARAMETERS["seq_length"] 
        self.num_epochs = FIXED_PARAMETERS["num_epochs"]
        self.alpha = FIXED_PARAMETERS["alpha"]
        self.test_matched_acc = 0

        model = "model"
        logger.Log("Building model from %s.py" %(model))
        self.model = MyModel(seq_length=self.sequence_length, emb_dim=self.embedding_dim, 
                                hidden_dim=self.dim, embeddings=loaded_embeddings, 
                                emb_train=self.emb_train,emb_train_topn=self.emb_train_topn)

        # Perform gradient descent with Adam
        self.optimizer = tf.train.AdamOptimizer(self.learning_rate, beta1=0.9, beta2=0.999).minimize(self.model.total_cost)

        # Boolean stating that training has not been completed, 
        self.completed = False 

        # tf things: initialize variables and create placeholder for session
        logger.Log("Initializing variables")
        self.init = tf.global_variables_initializer()
        self.sess = None
        self.saver = tf.train.Saver()


    def get_minibatch(self, dataset, start_index, end_index):
        datas = dataset[start_index:end_index]
        premise_vectors = np.vstack([data['premise_index_sequence'] for data in datas])
        hypothesis_vectors = np.vstack([data['hypothesis_index_sequence'] for data in datas])
        labels = [data['label'] for data in datas]
        return premise_vectors, hypothesis_vectors, labels


    def train(self, train_mnli, train_snli, dev_mat, test_matched):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.sess.run(self.init)

        self.best_dev_mat = 0.
        self.best_mtrain_acc = 0.
        self.last_train_acc = [.001, .001, .001, .001, .001]
        self.best_step = 0
        epoch = 0
        best_dev_acc = 0
        train_metrics,dev_metrics,test_metrics = 0,0,0

        # Restore most recent checkpoint if it exists. 
        # Also restore values for best dev-set accuracy and best training-set accuracy
        modname = FIXED_PARAMETERS["model_name"]
        ckpt_file = os.path.join(FIXED_PARAMETERS["ckpt_path"], modname) + ".ckpt"
        beta = int(self.alpha * len(train_snli))

        ### Training cycle
        logger.Log("Training...")
        logger.Log("Model will use %s percent of one-shot data during training" %(self.alpha * 100))

        training_data = train_mnli + random.sample(train_snli, beta)
        total_batch = math.ceil(len(training_data) / self.batch_size)

        while (epoch < self.num_epochs):
            random.shuffle(training_data)
            # Loop over all batches in epoch
            epoch_cost = 0.
            for i in range(total_batch):
                # Assemble a minibatch of the next B examples
                minibatch_premise_vectors, minibatch_hypothesis_vectors, minibatch_labels = self.get_minibatch(
                    training_data, self.batch_size * i, self.batch_size * (i + 1))
                
                # Run the optimizer to take a gradient step, and also fetch the value of the 
                # cost function for logging
                # modify to display f1 per batch
                feed_dict = {self.model.premise_x: minibatch_premise_vectors,
                                self.model.hypothesis_x: minibatch_hypothesis_vectors,
                                self.model.y: minibatch_labels, 
                                self.model.keep_rate_ph: self.keep_rate}
                _, c = self.sess.run([self.optimizer, self.model.total_cost], feed_dict)
                epoch_cost += c

            if epoch % self.display_epoch_freq == 0:
                mtrain_acc, mtrain_cost,train_metrics = evaluate_classifier(self.classify, train_mnli, self.batch_size)

                print("Epoch: ", epoch," Train Acc: ",train_metrics)


                                
            # Increment epoch count
            epoch += 1
            print("Cost (epoch) is: ", epoch_cost)

        self.completed = True
        #compute and print final stats
        mtrain_acc, mtrain_cost,train_metrics = evaluate_classifier(self.classify, train_mnli, self.batch_size)
        dev_acc_mat, dev_cost_mat,dev_metrics = evaluate_classifier(self.classify, dev_mat, self.batch_size)
        test_matched_acc, test_matched_cost,test_metrics = evaluate_classifier(self.classify, test_matched, self.batch_size)
        print("Final Epoch: ", epoch, "\nF1 metrics :  ", "\nTrain: ", train_metrics, "\nDev: ",                                                                            dev_metrics, "\nTest: ", test_metrics)


    def classify(self, examples,return_predictions=True):
        # This classifies a list of examples
        if (test == True) or (self.completed == True):
            best_path = os.path.join(FIXED_PARAMETERS["ckpt_path"], modname) + ".ckpt_best"
            self.sess = tf.Session()
            self.sess.run(self.init)
            self.saver.restore(self.sess, best_path)
            logger.Log("Model restored from file: %s" % best_path)

        total_batch = math.ceil(len(examples) / self.batch_size)
        logits = np.empty(2)
        for i in range(total_batch):
            minibatch_premise_vectors, minibatch_hypothesis_vectors, minibatch_labels = self.get_minibatch(examples,
                                    self.batch_size * i, self.batch_size * (i + 1))
            feed_dict = {self.model.premise_x: minibatch_premise_vectors, 
                                self.model.hypothesis_x: minibatch_hypothesis_vectors,
                                self.model.y: minibatch_labels, 
                                self.model.keep_rate_ph: 1.0}
            logit, cost = self.sess.run([self.model.logits, self.model.total_cost], feed_dict)
            logits = np.vstack([logits, logit])

        predictions = np.argmax(logits[1:], axis=1)
        if return_predictions:
            return predictions, cost
        else:
            for index, example in enumerate(examples):
                example["score_positive"] = logits[index][0]
                example["score_negative"] = logits[index][1]
                example["prediction"] = predictions[index]

    def classify(self, examples):
        # This classifies a list of examples
        total_batch = math.ceil(len(examples) / self.batch_size)
        logits = np.empty(2)
        total_cost = 0.
        for i in range(total_batch):
            minibatch_premise_vectors, minibatch_hypothesis_vectors, minibatch_labels = self.get_minibatch(examples,
                                                                                                           self.batch_size * i,
                                                                                                           self.batch_size * (
                                                                                                                       i + 1))
            feed_dict = {self.model.premise_x: minibatch_premise_vectors,
                         self.model.hypothesis_x: minibatch_hypothesis_vectors,
                         self.model.y: minibatch_labels,
                         self.model.keep_rate_ph: 1.0}
            logit, cost = self.sess.run([self.model.logits, self.model.total_cost], feed_dict)
            total_cost += cost
            logits = np.vstack([logits, logit])

        return np.argmax(logits[1:], axis=1), total_cost

    def restore(self, best=True):
        if True:
            path = os.path.join(FIXED_PARAMETERS["ckpt_path"], modname) + ".ckpt_best"
        else:
            path = os.path.join(FIXED_PARAMETERS["ckpt_path"], modname) + ".ckpt"
        self.sess = tf.Session()
        self.sess.run(self.init)
        self.saver.restore(self.sess, path)
        logger.Log("Model restored from file: %s" % path)



def make_predictions(candidates,threshold=0.1):
    dictpath = os.path.join(FIXED_PARAMETERS["log_path"], modname) + ".p"
    word_indices = pickle.load(open(dictpath, "rb"))
    print("Padding and indexifying sentences")
    sentences_to_padded_index_sequences(word_indices, [candidates])
    classifier = modelClassifier(FIXED_PARAMETERS["seq_length"])
    classifier.completed = True
    classifier.classify(candidates)
    # filter candidates and compute pseudo-confidence
    candidates = [cand for cand in candidates if (cand["prediction"] == 0)]
    total_score = 0.0
    for candidate in candidates:
        total_score += candidate["score_positive"]
    for candidate in candidates:
        candidate["confidence"] = candidate["score_positive"]/total_score
    candidates = sorted([cand for cand in candidates if (cand["confidence"] > threshold)],key=lambda x:x[
        "confidence"],reverse=True)

    return candidates



from utils import logger
tf.reset_default_graph()

FIXED_PARAMETERS = params.load_parameters()
random.seed(FIXED_PARAMETERS["SEED"])
np.random.seed(FIXED_PARAMETERS["SEED"])
tf.set_random_seed(FIXED_PARAMETERS["SEED"])

MODEL_NAME = "MODEL_NAME"
DATETIME = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

EXP_PATH = os.path.join("data",MODEL_NAME,str(FIXED_PARAMETERS["alpha"]),DATETIME)
if not (os.path.isdir(EXP_PATH)):
    os.makedirs(EXP_PATH)
FIXED_PARAMETERS["log_path"] = EXP_PATH
FIXED_PARAMETERS["ckpt_path"] = EXP_PATH

modname = FIXED_PARAMETERS["model_name"]
logpath = os.path.join(FIXED_PARAMETERS["log_path"], modname) + ".log"
logger = logger.Logger(logpath)

logger.Log("FIXED_PARAMETERS\n %s" % FIXED_PARAMETERS)

######################### LOAD DATA #############################

logger.Log("Loading data")


training_mnli = load_nli_data(FIXED_PARAMETERS["training_mnli"], shuffle=True)
training_snli = load_nli_data(FIXED_PARAMETERS["training_snli"], shuffle=True)
dev_matched = load_nli_data(FIXED_PARAMETERS["dev_matched"])
test_matched = load_nli_data(FIXED_PARAMETERS["test_matched"])

dictpath = os.path.join(FIXED_PARAMETERS["log_path"], "model") + ".p"

# if not os.path.isfile(dictpath):
logger.Log("Building dictionary")
if FIXED_PARAMETERS["alpha"] == 0:
    word_indices = build_dictionary([training_mnli])
else:
    word_indices = build_dictionary([training_mnli, training_snli])

logger.Log("Padding and indexifying sentences")
sentences_to_padded_index_sequences(word_indices, [training_mnli, training_snli,
                                                   dev_matched, test_matched])
pickle.dump(word_indices, open(dictpath, "wb"))

logger.Log("Loading embeddings")
loaded_embeddings = loadEmbedding_rand(FIXED_PARAMETERS["embedding_data_path"], word_indices)


classifier = modelClassifier(FIXED_PARAMETERS)

classifier.train(training_mnli, training_snli, dev_matched, test_matched)
