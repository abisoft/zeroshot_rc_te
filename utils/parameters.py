"""
from https://github.com/nyu-mll/multiNLI
"""

import io
import os
import json


def load_parameters():
    FIXED_PARAMETERS = {
        "model_type": "cim",
        "model_name": "cim",
        "training_mnli": "data/data_levy/train.txt",
        "training_snli": "data/data_levy/train_oneshot.txt",
        "dev_matched": "data/data_levy/dev.txt",
        "test_matched": "data/data_levy/eval.txt",
        "embedding_data_path": "data/glove.840B.300d.txt",
        "log_path": "data/log",
        "ckpt_path":  "data/log",
        "embeddings_to_load": None,
        "word_embedding_dim": 300,
        "hidden_embedding_dim": 50,
        "keep_rate": 0.9,
        "num_epochs": 5,
        "display_epoch_freq": 2,
        "batch_size": 256,
        "learning_rate": 0.005,
        "seq_length": 40,
        "emb_train": False,
        "emb_train_topn": False,
        "alpha": 0,
        "SEED":123
    }

    return FIXED_PARAMETERS