"""
from https://github.com/nyu-mll/multiNLI
"""
import csv
import sys

from utils.data_processing import tokenize
from sklearn.metrics import f1_score,precision_recall_fscore_support

def evaluate_classifier(classifier, eval_set, batch_size):
    """
    Function to get accuracy and cost of the model, evaluated on a chosen dataset.

    classifier: the model's classfier, it should return logit values, and cost for a given minibatch of the
    evaluation dataset
    eval_set: the chosen evaluation set, for eg. the dev-set
    batch_size: the size of minibatches.
    """
    correct = 0
    y_true = []
    y_pred = []
    hypotheses, total_cost = classifier(eval_set)
    cost = total_cost / batch_size
    full_batch = len(eval_set)
    for i in range(full_batch):
        i_label = eval_set[i]['label']
        y_true.append(i_label)
        hypothesis = hypotheses[i]
        y_pred.append(hypothesis)
        if hypothesis == i_label:
            correct += 1

    f1_score_default = f1_score(y_true,y_pred)

    return 100*correct / float(len(eval_set)), cost,f1_score_default


def evaluate_final(restore, classifier, eval_sets, batch_size):
    """
    Function to get percentage accuracy of the model, evaluated on a set of chosen datasets.
    
    restore: a function to restore a stored checkpoint
    classifier: the model's classfier, it should return logit values, and cost for a given minibatch of the
    evaluation dataset
    eval_set: the chosen evaluation set, for eg. the dev-set
    batch_size: the size of minibatches.
    """
    restore(best=True)
    percentages = []
    length_results = []
    for eval_set in eval_sets:
        bylength_prem = {}
        bylength_hyp = {}
        hypotheses, cost = classifier(eval_set)
        correct = 0
        cost = cost / batch_size

        for i in range(len(eval_set)):
            hypothesis = hypotheses[i]
            length_1 = len(tokenize(eval_set[i]['premise']))
            length_2 = len(tokenize(eval_set[i]['hypothesis']))
            if length_1 not in bylength_prem.keys():
                bylength_prem[length_1] = [0,0]
            if length_2 not in bylength_hyp.keys():
                bylength_hyp[length_2] = [0,0]

            bylength_prem[length_1][1] += 1
            bylength_hyp[length_2][1] += 1

            if hypothesis == eval_set[i]['label']:
                correct += 1  
                bylength_prem[length_1][0] += 1
                bylength_hyp[length_2][0] += 1    
        percentages.append(100*correct / float(len(eval_set)))
        length_results.append((bylength_prem, bylength_hyp))
    return percentages, length_results


def predictions_kaggle(classifier, eval_set, batch_size, name):
    """
    Get comma-separated CSV of predictions.
    Output file has two columns: pairID, prediction
    """
    INVERSE_MAP = {
    1: "POSITIVE",
    0: "NEGATIVE"
    }

    hypotheses = classifier(eval_set)
    predictions = []
    
    for i in range(len(eval_set)):
        hypothesis = hypotheses[i]
        prediction = INVERSE_MAP[hypothesis]
        pairID = eval_set[i]["id"]
        predictions.append((pairID, prediction))

    #predictions = sorted(predictions, key=lambda x: int(x[0]))

    f = open( name + '_predictions.csv', 'wb')
    w = csv.writer(f, delimiter = ',')
    w.writerow(['id','gold_label'])
    for example in predictions:
        w.writerow(example)
    f.close()
