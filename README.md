# Zero-shot relation classification


Code which can be used to extract relations given their descriptions, based on 
the approach described in [1]. It is based on the MultiNLI (https://github.com/nyu-mll/multiNLI) code used to establish  baselines for the MultiNLI corpus[2].

The dataset used can be found at https://drive.google.com/open?id=1iiviiy1vfypyeEvWYTc0Yh3Dye1PThme

Run with: `python -m te.train_mnli`
  
  
  
  
#### References
[1] Abiola Obamuyide and Andreas Vlachos."Zero-shot Relation Classification as Textual Entailment." In 
Proceedings of FEVER @ EMNLP 2018.

[2] Adina Williams, Nikita Nangia and Samuel Bowman. "A Broad-Coverage Challenge Corpus for Sentence Understanding 
through Inference." In Proceedings of NAACL 2018.

[3] Omer Levy, Minjoon Seo, Eunsol Choi and Luke Zettlemoyer. "Zero-Shot Relation Extraction via Reading Comprehension."
 In Proceedings of CoNLL 2017.